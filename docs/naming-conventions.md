# Naming Conventions

This document lists all the different naming conventions used in this project.

| file type | extension |
| ---       | ---       |
| YAML      | .yml      |
