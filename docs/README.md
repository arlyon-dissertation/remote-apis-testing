# Documentation

This folder gathers technical documentation of the project.  

+ [Naming Conventions](naming-conventions.md)
+ Running the project
  + [Setting up the environment](environment-setup.md)
  + [Setting up the cluster](cluster-setup.md)
+ Metrics
  + [Jaeger tracing](traces.md)
  + [Fluent-bit](fluent-bit.md)
+ [About incremental builds](incremental.md)
+ [Known issues and workarounds](workaround.md)
