# Incremental Builds

## What is an incremental build?

An incremental build is where a build of a newer version of some
software is done after building the previous version, using the cache
generated by the build of the previous version. This results in there
being a number of cached actions as well as a number of uncached ones.

## Why are we interested?

Doing incremental builds allows us to see relative caching performance
amongst the remote execution server implementaions. The caching is where
a significant portion of the speed increase from these kinds of setups
comes from. It is therefore important to show how well the caching works
for each of these implementations.

## What do we do?

In our tests we do incremental builds of the Bazel project, using the
Bazel client to run the builds. Initially we build Bazel 1.1.0, and then
once that has finished, without tearing down the remote execution
server, we move on to build Bazel 1.2.0.

## Notes on our configs

The config for our BuildGrid server had to be modified when introducing
incremental builds. This was due to the fact that its cache was
previously too small to cache a full build of Bazel. It initially had a
cache size of 1GB with a limit of cached refs set at 1024. These values
were increased to 4GB and 8192 respectively, as these values were tested
and known to provide a large enough cache for a Bazel build.
